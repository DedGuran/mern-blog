import React, { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Grid from '@mui/material/Grid';
import { useDispatch, useSelector } from 'react-redux';

import { Post } from '../../components/Post';

import { fetchTagsPosts, fetchPopularPosts } from '../../redux/slices/posts';


export const TagsPage = () => {
  const { name } = useParams();


  const dispatch = useDispatch();
  const userData = useSelector(state => state.auth.data)
  const { posts } = useSelector(state => state.posts)

  const [sortValue, setSortValue] = useState(0)

  const isPostsLoading = posts.status === 'loading'


  const loadTagsPosts = (tagName) => {
    dispatch(fetchTagsPosts(tagName))

    setSortValue(0)
  }
  const loadPopularTagsPosts = () => {
    dispatch(fetchPopularPosts())

    setSortValue(1)
  }
  useEffect(() => {
    loadTagsPosts(name)
  }, [])
  console.log('1', posts)
  return (
    <>
      <Tabs style={{ marginBottom: 15 }} value={sortValue} aria-label="basic tabs example">
        <Tab label="Новые" onClick={loadTagsPosts} />
        <Tab label="Популярные" onClick={loadPopularTagsPosts} />
      </Tabs>
      <Grid container spacing={4}>
        <h2>
          {name}
        </h2>
        <Grid container spacing={3} item>
          { }
          {(isPostsLoading ? [...Array(5)] : posts.items).map((data, index) => (isPostsLoading ? (

            <Grid item xs={12} sm={6}>
              <Post key={index} isLoading={true} />
            </Grid>) : (
            <Grid item xs={12} sm={6}>
              <Post
                id={data._id}
                title={data.title}
                imageUrl={data.imageUrl ? process.env.REACT_APP_BACKEND_URL + `${data.imageUrl}` : ''}
                user={data.user}
                createdAt={data.createdAt}
                viewsCount={data.viewsCount}
                commentsCount={3}
                tags={data.tags}

                isLoading={false}
                isEditable={userData?.userData._id === data.user._id}
              />
            </Grid>
          )

          ))}
        </Grid>

      </Grid>
    </>
  );
};
