import React, { useEffect, useState } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Grid from '@mui/material/Grid';
import { useDispatch, useSelector } from 'react-redux';

import { Post } from '../components/Post';
import { TagsBlock } from '../components/TagsBlock';
import { CommentsBlock } from '../components/CommentsBlock';
import { fetchPosts, fetchTags, fetchPopularPosts } from '../redux/slices/posts';


export const Home = () => {
  const dispatch = useDispatch();
  const userData = useSelector(state => state.auth.data)
  const { posts, tags } = useSelector(state => state.posts)

  const [sortValue, setSortValue] = useState(0)

  const isPostsLoading = posts.status === 'loading'
  const isTagsLoading = tags.status === 'loading'

  const loadPosts = () => {
    dispatch(fetchPosts())
    dispatch(fetchTags())
    setSortValue(0)
  }
  const loadPopularPosts = () => {
    dispatch(fetchPopularPosts())
    dispatch(fetchTags())
    setSortValue(1)
  }
  useEffect(() => {
    loadPosts()
  }, [])

  return (
    <>
      <Tabs style={{ marginBottom: 15 }} value={sortValue} aria-label="basic tabs example">
        <Tab label="Новые" onClick={loadPosts} />
        <Tab label="Популярные" onClick={loadPopularPosts} />
      </Tabs>
      <Grid container spacing={4}>
        <Grid xs={8} item>
          { }
          {(isPostsLoading ? [...Array(5)] : posts.items).map((data, index) => (isPostsLoading ? (
            <Post key={index} isLoading={true} />) : (<Post
              id={data._id}
              title={data.title}
              imageUrl={data.imageUrl ? process.env.REACT_APP_BACKEND_URL + `${data.imageUrl}` : ''}
              user={data.user}
              createdAt={data.createdAt}
              viewsCount={data.viewsCount}
              commentsCount={3}
              tags={data.tags}

              isLoading={false}
              isEditable={userData?.userData._id === data.user._id}
            />)

          ))}
        </Grid>
        <Grid xs={4} item>
          <TagsBlock items={tags.items} isLoading={isTagsLoading} />
          <CommentsBlock
            items={[
              {
                user: {
                  fullName: 'Вася Пупкин',
                  avatarUrl: 'https://mui.com/static/images/avatar/1.jpg',
                },
                text: 'Это тестовый комментарий',
              },
              {
                user: {
                  fullName: 'Иван Иванов',
                  avatarUrl: 'https://mui.com/static/images/avatar/2.jpg',
                },
                text: 'When displaying three lines or more, the avatar is not aligned at the top. You should set the prop to align the avatar at the top',
              },
            ]}
            isLoading={false}
          />
        </Grid>
      </Grid>
    </>
  );
};
